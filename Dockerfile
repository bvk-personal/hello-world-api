FROM node:11 as build
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile
COPY . .
RUN yarn build
RUN rm -rf node_modules

FROM node:11-alpine
WORKDIR /app
COPY --from=build /app /app
RUN yarn install --frozen-lockfile --production
RUN ls /app
ENTRYPOINT ["node", "/app/build/index.js"]