#! /bin/bash
PROJECT_ID=bvk-$1
REGION_ID=europe-west1
IMAGE=$2
CLOUD_RUN_SVC=$(echo "$2" | sed -r 's/:/-/g')

gcloud components install beta --quiet
gcloud components update --quiet
gcloud config set run/region $REGION_ID --quiet
gcloud auth configure-docker --quiet
gcloud components install docker-credential-gcr --quiet
# docker build . --tag gcr.io/$PROJECT_ID/$IMAGE
# docker push gcr.io/$PROJECT_ID/$IMAGE

# gcloud beta run deploy $CLOUD_RUN_SVC --image gcr.io/$PROJECT_ID/$IMAGE --allow-unauthenticated --platform managed --quiet
URL=$(gcloud beta run services describe $CLOUD_RUN_SVC --format json --platform managed | jq .status.url -r)

echo Service deployed to $URL

curl -s $URL | jq .

# gcloud logging read "resource.type=cloud_run_revision AND resource.labels.service_name=$CLOUD_RUN_SVC AND severity>=DEFAULT" --format json --limit=1 | jq .

echo Logs are available at https://console.cloud.google.com/run/detail/${REGION_ID}/${CLOUD_RUN_SVC}/logs?project=${PROJECT_ID}