import { LoggerOptions } from 'bunyan'
import { Type } from 'class-transformer'
import { IsDefined, IsNumber, Max, Min, ValidateNested } from 'class-validator'
class Server {
  @IsDefined()
  @IsNumber()
  @Max(65536)
  @Min(1024)
  public port: number
}
export class Config {
  @IsDefined()
  public logger: LoggerOptions

  @IsDefined()
  @ValidateNested()
  @Type(() => Server)
  public server: Server
}
