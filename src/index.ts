import { loadConfig } from '@buynomics/bn-config'
import bunyan from 'bunyan'
import Koa from 'koa'
import 'reflect-metadata'
import { Config } from './config'
const config = loadConfig(Config)
const logger = bunyan.createLogger(config.logger)
const app = new Koa()
app.use(async ctx => {
  ctx.body = {
    message: 'Hello World',
  }
})
const start = async () => {
  app.listen(process.env.PORT || config.server.port, () => {
    logger.info('Started bits-api')
  })
}
start()
